var express = require("express");
var bodyParser = require("body-parser");
var router = express.Router();
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({extended:true});
var transportType ;
var carburantType ;
var nbKmVoiture ;
var nbKmAvion ;
var dieselDepense = 133 ; // 133g de co2/km pour une voiture diesel d'une moyenne de 5L/100km
var essenceDepense = 118 ;
var avionDepense = 285 ;
var transportsCommunDepense = 68 ; //68g par passager
var trainDepense = 52;




router.get("/", function(req, res) {
   // console.log("hello I'm on the start page");
res.render("index");
});

router.get("/infos", function(req, res) {
   // console.log("hello I'm on the start page");
res.render("details.ejs");
});

router.post('/', urlencodedParser, function(req, res) {
   console.log(req.body);
   transportType = req.body.transport;
   nbKmVoiture = req.body.kmVoiture;
   carburantType = req.body.carburant ;
   nbKmAvion = req.body.kmAvion;
   console.log(transportType+nbKmVoiture+carburantType+nbKmAvion);
   console.log('empreinteurlencodedParser transports ='+calculEmpreinteTransports(transportType,nbKmVoiture,carburantType,nbKmAvion));
   res.render("index");
   res.end();
 });

 router.get('/objects',function(req,res){
   let obj = {
      id : 1 ,
      name : 'name',
   }
   return res.status(200).json(obj);
 })

 router.post('/transports',urlencodedParser,function(req,res){
    console.log(req);
   transportType = req.body.transport;
   nbKmVoiture = parseInt(req.body.kmVoiture);
   carburantType = req.body.carburant ;
   nbKmAvion = parseInt(req.body.kmAvion);
   console.log("avion",nbKmAvion,"voiture",nbKmVoiture,"transport",transportType,"carb",carburantType);
   let reponse = calculEmpreinteTransports2(transportType,nbKmVoiture,carburantType,nbKmAvion)
   return res.status(200).json(reponse);
   
 })

 function calculEmpreinteTransports2(transportType,nbKmVoiture,carburantType,nbKmAvion){
    var depenseVoiture = 0;
    var depenseCommun = 0 ;
    var depenseAvion = avionDepense*nbKmAvion;
    var depenseTotale = 0 ;
    if(transportType == 'voiture'){
       if(carburantType == 'diesel'){
          depenseVoiture = nbKmVoiture * dieselDepense ;
       }
       else{ depenseVoiture = nbKmVoiture * essenceDepense ;}
       depenseTotale =depenseVoiture + depenseAvion ;
    }
    else if (transportType == 'commun') {
    depenseCommun = transportsCommunDepense * nbKmVoiture ;
    depenseTotale = depenseCommun + depenseAvion ;
 }
    else if (transportType == 'train'){
      var depenseTrain = trainDepense * nbKmVoiture ;
      depenseTotale = depenseTrain+depenseAvion;
    }
    else{ depenseTotale = depenseAvion ;}
   
    console.log("voiture",depenseVoiture,"totale",depenseTotale,"avion",depenseAvion,"commun ",depenseCommun);
    return {
       depenseC02 : depenseTotale
    };


 }


 function calculEmpreinteTransports(transportType,nbKmVoiture,carburantType,nbKmAvion){
   var depenseVoiture ;
   var depenseCommun ;
   var depenseAvion = avionDepense*nbKmAvion;
   if(transportType == 'voiture'){
      if(carburantType == 'diesel'){
         depenseVoiture = nbKmVoiture * dieselDepense ;
      }
      depenseVoiture = nbKmVoiture * essenceDepense ;
      return depenseVoiture + depenseAvion ;
   }
   else if (transportType == 'commun') {
   depenseCommun = transportsCommunDepense * nbKmVoiture ;
   return depenseCommun + depenseAvion ;
}
   else if (transportType == 'train'){
     var depenseTrain = trainDepense * nbKmVoiture ;
     return depenseTrain+depenseAvion;
   }
   return depenseAvion ;
   
 }

module.exports = router;